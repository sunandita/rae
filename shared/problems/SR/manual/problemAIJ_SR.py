__author__ = 'patras'

from domain_searchAndRescue import *
from timer import DURATION
from state import state

def GetCostOfMove(r, l1, l2, dist):
    return dist

DURATION.COUNTER = {
    'giveSupportToPerson': 15,
    'clearLocation': 5,
    'inspectPerson': 20,
    'moveEuclidean': GetCostOfMove,
    'moveCurved': GetCostOfMove,
    'moveManhattan': GetCostOfMove,
    'fly': 15,
    'inspectLocation': 5,
    'transfer': 2,
    'replenishSupplies': 4,
    'changeAltitude': 2,
    'captureImage': 1,
 }

DURATION.COUNTER = {
    'giveSupportToPerson': 15,
    'clearLocation': 5,
    'inspectPerson': 20,
    'moveEuclidean': GetCostOfMove,
    'moveCurved': GetCostOfMove,
    'moveManhattan': GetCostOfMove,
    'fly': 15,
    'inspectLocation': 5,
    'transfer': 2,
    'replenishSupplies': 4,
    'changeAltitude': 2,
    'captureImage': 1,
 }


rv.WHEELEDROBOTS = ['w1', 'w2']
rv.DRONES = ['a1', 'a2']
rv.OBSTACLES = { (100, 100)}
  
def ResetState():  
	# initial values state variables 
	state.loc = {'w1': (15,15), 'w2': (29,29), 'p1': (28,30), 'p2': (10,30), 'a1': (9,19), 'a2': (4,5)}
	state.hasMedicine = {'a1': 0, 'a2': 0, 'w1': 0, 'w2': 0}
	state.robotType = {'w1': 'wheeled', 'a1': 'uav', 'a2': 'uav', 'w2': 'wheeled'}
	state.status = {'w1': 'free', 'w2': 'free', 'a1': UNK, 'a2': UNK, 'p1': UNK, 'p2': UNK, (28,30): UNK, (15, 15): UNK, (10, 30): UNK}
	state.altitude = {'a1': 'high', 'a2': 'low'}
	state.currentImage = {'a1': None, 'a2': None}
	state.newRobot = {1: None}

	# properties of the environment 
	state.realStatus = {'w1': 'OK', 'p1': 'OK', 'p2': 'injured', 'w2': 'OK', 'a1': 'OK', 'a2': 'OK', (28, 30): 'hasDebri', (15, 15): 'clear', (10, 30): 'hasDebri'}
	state.realPerson = {(28,30): 'p1', (15, 15): None, (10, 30): 'p2'}
	state.weather = {(28,30): "foggy", (15, 15): "rainy", (10, 30): "dustStorm"}

# tasks to accomplish 
tasks = {
    8: [['survey', 'a1', (15,15)], ['survey', 'a2', (28,30)]],
    20: [['survey', 'a1', (10,30)]]
}

eventsEnv = {
}

